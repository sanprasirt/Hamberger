TARGET=hello

all: ${TARGET}

hello: ${TARGET}.c
	echo "#define TODAY \"`date`\"" | tee hello.h
	${CC} ${TARGET}.c -o ${TARGET}

clean:
	rm ${TARGET}